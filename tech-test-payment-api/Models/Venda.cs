﻿using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
   
  
    public class Venda
    {
        [Required]
        public int Id { get;internal set; }
        [Required]
        public Vendedor Vendedor { get; set; }
        [Required]
        public DateTime DataDaVenda { get; set; }
        public List<Item> Item { get; set; }
        public EnumStatusVenda Status { get; set; }
    }

    public class Vendedor
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string CPF { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Telefone { get; set; }

    }
    public class Item
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public int VendaId { get; set; }
        public string Nome { get; set; }
        [JsonPropertyName("Quantidade")]
        public int Qtd { get; set; }
    }

    public enum EnumStatusVenda
    {
        Aguardando,
        Cancelada,
        Aprovado,
        Enviado,
        Entregue
    }


}
