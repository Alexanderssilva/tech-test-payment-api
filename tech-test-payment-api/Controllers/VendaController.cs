﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("Registrar Venda")]
        public IActionResult CriarVenda(Venda venda)
        {
            var novaVenda = new Venda();
            novaVenda.Status = EnumStatusVenda.Aguardando;
            novaVenda.DataDaVenda = DateTime.Now;
         
            var vendedor = _context.Vendedores.Find(venda.Vendedor.Id);
            if (vendedor == null)
            {
                try
                {
                    var novoVendedor = new Vendedor();
                    novoVendedor.CPF = venda.Vendedor.CPF;
                    novoVendedor.Email = venda.Vendedor.Email;
                    novoVendedor.Telefone = venda.Vendedor.Telefone;
                    novoVendedor.Nome = venda.Vendedor.Nome;
                    _context.Vendedores.Add(novoVendedor);
                    _context.SaveChanges();
                    vendedor = novoVendedor;
                }
                catch
                {
                    return BadRequest(new { Erro = "É necessário adicionar todos os campos de vendedor" });
                }
            }

            if(venda.Item.Count == 0 || venda.Item == null)
                return BadRequest(new { Erro = "É necessário adicionar todos os campos de vendedor" });

            novaVenda.Vendedor = vendedor;
            _context.Vendas.Add(novaVenda);
            _context.SaveChanges();

            foreach (var item in venda.Item)
            {
                item.VendaId = novaVenda.Id;
                _context.Itens.Add(item);
                _context.SaveChanges();

            }
             return CreatedAtAction(nameof(BuscarVenda), new { id = novaVenda.Id }, novaVenda);
        }

        [HttpGet("Buscar-Venda/id Venda")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = new Venda();
            var vendedor = new Vendedor();
            var item = new List<Item>();

            venda = _context.Vendas.Find(id);
            var idVendedor = _context.Vendas.Where(c => c.Id == id).Select(p => p.Vendedor.Id).First();

            if (venda == null) return NotFound();

            item = _context.Itens.Where(x => x.VendaId == id).ToList();
            if (item == null) return NotFound();

            vendedor = _context.Vendedores.FirstOrDefault(c => c.Id == idVendedor);
            if (vendedor == null) return NotFound();

            var retorno = new Venda();
            retorno.Id = venda.Id;
            retorno.DataDaVenda = venda.DataDaVenda;
            retorno.Status = venda.Status;
            retorno.Vendedor = vendedor;
            retorno.Item = item;
                 
            return Ok(retorno);
        }

        [HttpPut("Atualizar-Venda/id Venda")]
        public IActionResult AtualizarVenda(int id,EnumStatusVenda statusVenda)
        {

            var venda = new Venda();
            var vendedor = new Vendedor();
            var item = new List<Item>();

            venda = _context.Vendas.Find(id);
            if (venda == null) return NotFound();
            var idVendedor = _context.Vendas.Where(c => c.Id == id).Select(p => p.Vendedor.Id).First();


            item = _context.Itens.Where(x => x.VendaId == id).ToList();
            if (item == null) return NotFound();

            vendedor = _context.Vendedores.FirstOrDefault(c => c.Id == idVendedor);
            if (vendedor == null) return NotFound();

            venda.Vendedor = vendedor;
            venda.Item = item;

            switch (venda.Status)
            {
                case EnumStatusVenda.Aguardando:
                    if(statusVenda == EnumStatusVenda.Aprovado)
                    {
                        venda.Status = EnumStatusVenda.Aprovado;
                    }
                    else if(statusVenda == EnumStatusVenda.Cancelada)
                    {
                        venda.Status = EnumStatusVenda.Cancelada;
                    }
                    else
                    {
                        return BadRequest(new { Erro = "A venda precisa estar em outra situação para atualizar" });

                    }
                    break;
                case EnumStatusVenda.Aprovado:
                    if (statusVenda == EnumStatusVenda.Enviado)
                    {
                        venda.Status = EnumStatusVenda.Enviado;
                    }
                    else if (statusVenda == EnumStatusVenda.Cancelada)
                    {
                        venda.Status = EnumStatusVenda.Cancelada;
                    }
                    else
                    {
                        return BadRequest(new { Erro = "A venda precisa estar em outra situação para atualizar" });

                    }
                    break;
                case EnumStatusVenda.Enviado:
                    if (statusVenda == EnumStatusVenda.Entregue)
                    {
                        venda.Status = EnumStatusVenda.Entregue;
                    }
                    else
                    {
                        return BadRequest(new { Erro = "A venda precisa estar em outra situação para atualizar" });

                    }
                    break;
                default:
                    return BadRequest(new { Erro = "A venda precisa estar em outra situação para atualizar" });
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id }, venda);
        }

    }
}
